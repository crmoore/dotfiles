

local function openIterm()
	hs.osascript.applescript([[
		if application "iTerm" is not running then
			activate application "iTerm"
		end if
		tell application "iTerm"
			create window with default profile
		end tell
	]])
end

hs.hotkey.bind({"cmd","shift"}, "space", "Enter Command Mode", function()
	local esckey
	local keys = {}
	local bind = function(mod, key, fn)
		key = hs.hotkey.bind(mod, key, function()
			fn()
			hs.fnutils.each(keys, function(k)
				k:disable()
			end)
			esckey:disable()
		end)
		table.insert(keys,key)
	end

	bind( {}, "1", function()
		local win = hs.window.focusedWindow()

		hs.grid.setGrid("2x2")
		hs.grid.set( win, "0,0 1x2")
	end)

	bind({}, "2", function()
		local win = hs.window.focusedWindow()
		hs.grid.setGrid("2x2")
		hs.grid.set( win, "1,0 1x2")
	end)

	bind( {}, "3", function()
		local win = hs.window.focusedWindow()
		hs.grid.setGrid("3x3")
		hs.grid.set( win, "2,0 1x1")
	end)

	bind( {}, "4", function()
		local win = hs.window.focusedWindow()
		hs.grid.setGrid("3x3")
		hs.grid.set( win, "2,1 1x1")
	end)

	bind( {}, "5", function()
		local win = hs.window.focusedWindow()
		hs.grid.setGrid("3x3")
		hs.grid.set( win, "2,2 1x1")
	end)

	bind( {}, "6", function()
		local win = hs.window.focusedWindow()
		hs.grid.setGrid("3x3")
		hs.grid.set( win, "1,0 1x2")
	end)

	bind( {}, "7", function()
		local win = hs.window.focusedWindow()
		hs.grid.setGrid("3x3")
		hs.grid.set( win, "1,2 1x1")
	end)

	bind( {}, "8", function()
		local win = hs.window.focusedWindow()
		hs.grid.setGrid("3x3")
		hs.grid.set( win, "2,0 1x2")
	end)

	bind( {}, "space", function()
		hs.grid.setGrid("3x3")
		hs.grid.show()
	end)

	bind({}, "i", function()
		hs.hints.windowHints()
	end)

	bind({}, "t", function()
		openIterm()
	end)

	esckey = hs.hotkey.bind({}, "escape", "Exit Command Mode", function()
		hs.fnutils.each(keys, function(k)
			k:disable()
		end)
		esckey:disable()
	end)
end)

-----------------------------------------------
-- Reload config on write
-----------------------------------------------

function reload_config(files)
    hs.reload()
end
hs.pathwatcher.new(os.getenv("HOME") .. "/.hammerspoon/", reload_config):start()
hs.alert.show("Config loaded")
