" workaround to silence py3 deprecation warning (imp module) until fixed in
" upstream vim
if has('python3')
	silent! python3 1
endif

" == Pathogen {{{
let g:pathogen_disabled = []
call add(g:pathogen_disabled, 'project')   " unused

call add(g:pathogen_disabled, 'xmledit')   " very annoying
"call add(g:pathogen_disabled, 'ctrlp') " replaced with unite
call add(g:pathogen_disabled, 'unite') " replaced with ctrlp
call add(g:pathogen_disabled, 'YouCompleteMe') "

filetype on  " may not be needed
filetype off " may not be needed
call pathogen#infect()
filetype plugin indent on
syn on

set rtp+=~/Applications/homebrew/opt/fzf
"}}}

" == Gui/Colors {{{
if has("gui_running")
	set guioptions-=T
	set background=dark
	colorscheme solarized
	set macligatures
	set guifont=Fira\ Code:h12
else
	set background=dark
	"let g:solarized_termcolors=256
	let g:solarized_termcolors=16
	"let g:solarized_visibility = "high"
	"let g:solarized_contrast = "high"
	colorscheme solarized
endif
"}}}

" == Basic settings {{{
set ai
set hls
set laststatus=2

set hidden

set cursorline
" hi CursorLine term=none cterm=none ctermbg=lightgrey

" number lines
set number

set incsearch
set scrolloff=1
set backspace=indent,eol,start
set showcmd
set wildmenu
set autoread
set history=1000
set tabpagemax=50
"set diffopt+=vertical

" show whitespace {{{
"set list
"set listchars=tab:▸\ ,extends:❯,precedes:❮,nbsp:␣,eol:→
"set listchars=tab:⇥\ ,nbsp:·,trail:␣,extends:▸,precedes:◂
set lcs=tab:»\ 
set lcs+=trail:·
"}}}

set showmatch
set matchpairs+=<:>

" tabs {{{
set tabstop=4
set shiftwidth=4
set softtabstop=0
set copyindent
set preserveindent
set cinoptions=(0,u0,U0
"set expandtab
" }}}

set lazyredraw

" modelines {{{
set modeline
set modelines=5
"}}}

" set status line {{{
set statusline=
set statusline+=%<\                       " cut at start
set statusline+=%2*[%n%H%M%R%W]%*\        " buffer number, and flags
set statusline+=%-40f\                    " relative path
set statusline+=%=                        " seperate between right- and left-aligned
set statusline+=%1*%y%*%*\                " file type
set statusline+=%2c%10((%l/%L)%)\            " line and column
set statusline+=%P                        " percentage of file
"}}}

" ignore whitespace in diffs
"set diffopt+=iwhite

" text replacements {{{
iab NuM 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
iab RuL ----+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
"}}}

" print options {{{
set printoptions=left:5pc,header:0,paper:letter
set printfont=Monaco:h8
"}}}

"set relativenumber
set colorcolumn=120
"}}}

" == CTAGS {{{
" Run ctags:
" /opt/local/bin/ctags -R --c++-kinds=+p --fields=+iaS --extra=+q apps/ deprecated/ devices/ fm/ gnc/ io/ rt/ sim/ simv_server/ tests/ util/
" /opt/local/bin/ctags -f libs/boost.tags -R --c++-kinds=+p --fields=+iaS --extra=+q libs/boost_1_43_0/
"}}}

" == close preview window automaticly {{{
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif
" }}}

" == language-specific settings {{{
au FileType python setlocal expandtab list
au FileType haskell setlocal expandtab list
au FileType cabal setlocal expandtab list
au FileType javascript setlocal expandtab list tabstop=2 softtabstop=2 shiftwidth=2
"}}}

" == largefiles {{{
" Protect large files from sourcing and other overhead.
" Files become read only
if !exists("my_auto_commands_loaded")
  let my_auto_commands_loaded = 1
  " Large files are > 10M
  " Set options:
  " eventignore+=FileType (no syntax highlighting etc
  " assumes FileType always on)
  " noswapfile (save copy of file)
  " bufhidden=unload (save memory when other file is viewed)
  " buftype=nowritefile (is read-only)
  " undolevels=-1 (no undo possible)
  let g:LargeFile = 1024 * 1024 * 50
  augroup LargeFile
    autocmd BufReadPre * let f=expand("<afile>") | if getfsize(f) > g:LargeFile | set eventignore+=FileType | setlocal noswapfile bufhidden=unload buftype=nowrite undolevels=-1 | else | set eventignore-=FileType | endif
  augroup END
endif
"}}}

" == .vimsettings {{{
" Search for any .vimsettings files in the path to the file.
" Source them if you find them.
"   http://www.kornerstoane.com/2014/06/why-i-cant-stop-using-vim/
"   http://www.reddit.com/r/vim/comments/12tpf9/per_project_vim_settings/
"   https://github.com/thinca/vim-localrc
function! ApplyLocalSettings(dirname)
    " Don't try to walk a remote directory tree -- takes too long, too many
    " what if's
    let l:netrwProtocol = strpart(a:dirname, 0, stridx(a:dirname, "://"))
    if l:netrwProtocol != ""
        return
    endif

    " Convert windows paths to unix style (they still work)
    let l:curDir = substitute(a:dirname, "", "/", "g")
    let l:parentDir = strpart(l:curDir, 0, strridx(l:curDir, "/"))
    if isdirectory(l:parentDir)
        call ApplyLocalSettings(l:parentDir)
    endif

	" Now walk back up the path and source .vimsettings as you find them.  This
    " way child directories can 'inherit' from their parents
    let l:settingsFile = a:dirname . "/.vimsettings"
    if filereadable(l:settingsFile)
        exec ":source " . l:settingsFile
    endif
endfunction
autocmd! BufEnter * call ApplyLocalSettings(expand("<afile>:p:h"))
"}}}

" == silversearcher as grepcmd {{{
if executable('ag')
    " Note we extract the column as well as the file and line number
    set grepprg=ag\ --nogroup\ --nocolor\ --column
    set grepformat=%f:%l:%c%m
    let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
    let g:ctrlp_use_caching = 0
endif
"}}}

" == Clang Format {{{
map <Leader>f :pyf /Users/cmoore/Applications/homebrew/opt/clang-format/share/clang/clang-format.py<cr>
" }}}

" == EasyAlign {{{
" Start interactive EasyAlign in visual mode (e.g. vip<Leader>a)
vmap <Leader>a <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. <Leader>aip)
nmap <Leader>a <Plug>(EasyAlign)
"}}}

" == NERDCommenter {{{
let g:NERDSpaceDelims = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
" }}}

" == Unite {{{
"let g:unite_source_history_yank_enable = 1
"call unite#filters#matcher_default#use(['matcher_fuzzy'])
"nnoremap <leader>t :<C-u>Unite -no-split -buffer-name=files   -start-insert file_rec/async:.<cr>
"nnoremap <leader>f :<C-u>Unite -no-split -buffer-name=files   -start-insert file<cr>
"nnoremap <leader>r :<C-u>Unite -no-split -buffer-name=mru     -start-insert file_mru<cr>
"nnoremap <leader>o :<C-u>Unite -no-split -buffer-name=outline -start-insert outline<cr>
"nnoremap <leader>y :<C-u>Unite -no-split -buffer-name=yank    history/yank<cr>
"nnoremap <leader>e :<C-u>Unite -no-split -buffer-name=buffer  buffer<cr>
"
"" Custom mappings for the unite buffer
"autocmd FileType unite call s:unite_settings()
"function! s:unite_settings()
"  " Play nice with supertab
"  let b:SuperTabDisabled=1
"  " Enable navigation with control-j and control-k in insert mode
"  imap <buffer> <C-j>   <Plug>(unite_select_next_line)
"  imap <buffer> <C-k>   <Plug>(unite_select_previous_line)
"endfunction
"}}}

" == man {{{
runtime ftplugin/man.vim
set keywordprg=:Man
"}}}

" vim:foldmethod=marker:foldlevel=0
