" Vim syntax file
" Language:	User script for Lenze motor controller
" Version Info: 
" Author:       Chris R Moore <cmoore@vctinc.com>


if exists("b:current_syntax")
	finish
endif

" shut case off
syn case ignore

" comments are ; to eol"
syn match  lenzeComment	";.*$"
syn match  lenzeLabel	"[^;].*:"

"integer number, or floating point number without a dot.
syn match  basicNumber          "\<\d\+\>"
"floating point number, with dot
syn match  basicNumber          "\<\d\+\.\d*\>"
"floating point number, starting with a dot
syn match  basicNumber          "\.\d\+\>"


syn keyword lenzeKeywords define if else endif gosub return end event endevent
syn keyword lenzeKeywords do loop until enable on fault endfault resume time

syn match lenzeVar "V[0123456789]"
syn keyword lenzeRegs OUT1 OUT2 OUT3 OUT4 IREF IN_B1 IN_B2 AIN1 AOUT1 AOUT

hi def link lenzeKeywords Statement
hi def link lenzeLabel    Label
hi def link lenzeComment  Comment
hi def link basicNumber   Number
hi def link lenzeRegs     Identifier
hi def link lenzeVar      Identifier


let b:current_syntax = "lenze"
