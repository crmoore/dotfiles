
" highlight vct log files
syn match VCT_Emerg     /.*| EMERG |.*/
syn match VCT_Alert     /.*| ALERT |.*/
syn match VCT_Crit      /.*| CRIT  |.*/
syn match VCT_Error     /.*| ERROR |.*/
syn match VCT_Warn      /.*| WARN  |.*/
syn match VCT_Notice    /| NOTICE|/
syn match VCT_Info      /| INFO  |/
syn match VCT_Debug     /| DEBUG |/

highlight VCT_Emerg  cterm=bold term=bold ctermbg=red  ctermfg=white gui=bold guibg=red guifg=white
highlight VCT_Alert  cterm=bold term=bold ctermbg=red  ctermfg=white gui=bold guibg=red guifg=white
highlight VCT_Crit   cterm=bold term=bold ctermbg=NONE ctermfg=red   gui=bold guifg=red guibg=NONE
highlight VCT_Error  cterm=bold term=bold ctermbg=NONE ctermfg=red   gui=bold guifg=red guibg=NONE

highlight VCT_Warn   cterm=bold term=bold ctermbg=NONE ctermfg=yellow gui=bold guifg=yellow guibg=NONE
highlight VCT_Notice cterm=bold term=bold ctermbg=NONE ctermfg=green  gui=bold guifg=green  guibg=NONE
highlight VCT_Info   cterm=bold term=bold ctermbg=NONE ctermfg=cyan   gui=bold guifg=cyan   guibg=NONE
highlight VCT_Debug  cterm=bold term=bold ctermbg=NONE ctermfg=blue   gui=bold guifg=blue   guibg=NONE

